# dtk-vr

A port of `is`, GLSL >= #150 compliant.

### Dependencies

To install:

``` fish
conda env create -f ./pkg/env/dtk-vr.yaml
conda activate dtk-vr
```

To update:

``` fish
conda env update -f ./pkg/env/dtk-vr.yaml
```

And:

`vrpn`

NOTE: We should provide a `dtk-forge` `vrpn` package. That much is a no brainer.

**MacOSX**

``` fish
brew install vrpn
```

### Dependencies to come

- dtk-log  # in progress
- dtk-core # in progress

### TODO

- Quad Buffer Stereo
- Use `dtk-log`
- Implementation of the `Core` layer:
  - `dtkVrApplication`: as an overload of `dtkGuiApplication`, with the `ini` system
- Implementation of the `Tracking` layer
- Porting of the tracking algorithm
- the `tablet` `dtk-quick` && `dtk-cross` compliant `iOS` & `Android` controllers using `QtGamePad`
- Build some `VRPN` clients reflecting the controllers
- Extend the architecture to use the latter
