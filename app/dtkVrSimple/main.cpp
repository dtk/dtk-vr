// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtGui>

#include <dtkVrCore/dtkVrApplication.h>
#include <dtkVrCore/dtkVrView.h>

#include "dtkVrSimpleRenderer.h"

int main(int argc, char **argv)
{
     dtkVrApplication::initializeGL(argc, argv);

     dtkVrApplication application(argc, argv);
     application.initialize();

     dtkVrView view(instanciator<dtkVrSimpleRenderer>);
     view.showFullScreen();
     view.raise();

     return application.exec();
}

//
// main.cpp ends here
