// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVrCore/dtkVrRenderer.h>

#include <dtkVrModels/dtkVrModel.h>

class dtkVrSimpleRenderer : public dtkVrRenderer
{
    Q_OBJECT

public:
     dtkVrSimpleRenderer(void);
    ~dtkVrSimpleRenderer(void);

public:
    void   initializeGL(void)     override;
    void uninitializeGL(void)     override;
    void         drawGL(void)     override;
    void       resizeGL(int, int) override;

public:
    void mouseMoveEvent(QMouseEvent *)    override;
    void mousePressEvent(QMouseEvent *)   override;
    void mouseReleaseEvent(QMouseEvent *) override;

    void keyPressEvent(QKeyEvent *)   override;

private:
     class dtkVrSimpleRendererPrivate *d;
};

//
// dtkVrSimpleRenderer.h ends here
