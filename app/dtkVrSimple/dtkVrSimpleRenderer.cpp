// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrSimpleRenderer.h"

#include <dtkVr/dtkVrConfig.h>

#include <dtkVrCore/dtkVrRenderer.h>

#include <QtDebug>
#include <QtOpenGL>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

struct VertexData
{
    QVector3D position;
    QVector2D texCoord;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkVrSimpleRendererPrivate : protected QOpenGLExtraFunctions
{
public:
    QOpenGLBuffer *arrayBuf = nullptr;
    QOpenGLBuffer *indexBuf = nullptr;
    QOpenGLTexture *texture = nullptr;
#if !defined(Q_OS_MAC)
    QOpenGLDebugLogger *debugger = nullptr;
#endif
    QOpenGLShaderProgram program;

public:
    QMatrix4x4 projection;

public:
    void cube(void);
    void draw(void);

public:
    int width;
    int height;

private:
    friend class dtkVrSimpleRenderer;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkVrSimpleRenderer::dtkVrSimpleRenderer(void) : dtkVrRenderer()
{
    d = new dtkVrSimpleRendererPrivate;

    d->width = 2560;
    d->height = 1440;

    d->arrayBuf = new QOpenGLBuffer;
    d->indexBuf = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
}

dtkVrSimpleRenderer::~dtkVrSimpleRenderer(void)
{
    d->arrayBuf->destroy();
    d->indexBuf->destroy();

    delete d->arrayBuf;
    delete d->indexBuf;

#if !defined(Q_OS_MAC)
    delete d->debugger;
#endif
   
    delete d->texture;
    delete d;
}

void dtkVrSimpleRenderer::initializeGL(void)
{
    dtkVrRenderer::initializeGL();

    d->initializeOpenGLFunctions();

// /////////////////////////////////////////////////////////////////////////////
// Initialise logger
// /////////////////////////////////////////////////////////////////////////////

#if !defined(Q_OS_MAC)
    d->debugger = new QOpenGLDebugLogger(this);
    d->debugger->initialize();
    d->debugger->startLogging();
#endif

// /////////////////////////////////////////////////////////////////////////////
// Initialise buffers
// /////////////////////////////////////////////////////////////////////////////

    d->arrayBuf->create();
    d->indexBuf->create();

// /////////////////////////////////////////////////////////////////////////////
// Initialise shaders
// /////////////////////////////////////////////////////////////////////////////

    if(!d->program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":dtkVrSimple.v.glsl"))
        qFatal("Unable to read vertex shader");

    if(!d->program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":dtkVrSimple.f.glsl"))
        qFatal("Unable to read fragmentshader");

    if(!d->program.link())
        qFatal("Unable to link program");

    if(!d->program.bind())
         qFatal("Unable to bind program");

// /////////////////////////////////////////////////////////////////////////////
// Initialise textures
// /////////////////////////////////////////////////////////////////////////////

    d->texture = new QOpenGLTexture(QImage(":dtkVrSimple.cube.png").mirrored());
    d->texture->setMinificationFilter(QOpenGLTexture::Nearest);
    d->texture->setMagnificationFilter(QOpenGLTexture::Linear);
    d->texture->setWrapMode(QOpenGLTexture::Repeat);

// /////////////////////////////////////////////////////////////////////////////
// Initialise geometry
// /////////////////////////////////////////////////////////////////////////////

    d->cube();
}

void dtkVrSimpleRendererPrivate::cube(void)
{
    VertexData vertices[] = {
        {QVector3D(-50.0f, -50.0f,  50.0f), QVector2D(0.0f, 0.0f)},  // v0
        {QVector3D( 50.0f, -50.0f,  50.0f), QVector2D(0.33f, 0.0f)}, // v1
        {QVector3D(-50.0f,  50.0f,  50.0f), QVector2D(0.0f, 0.5f)},  // v2
        {QVector3D( 50.0f,  50.0f,  50.0f), QVector2D(0.33f, 0.5f)}, // v3

        {QVector3D( 50.0f, -50.0f,  50.0f), QVector2D( 0.0f, 0.5f)}, // v4
        {QVector3D( 50.0f, -50.0f, -50.0f), QVector2D(0.33f, 0.5f)}, // v5
        {QVector3D( 50.0f,  50.0f,  50.0f), QVector2D(0.0f, 1.0f)},  // v6
        {QVector3D( 50.0f,  50.0f, -50.0f), QVector2D(0.33f, 1.0f)}, // v7

        {QVector3D( 50.0f, -50.0f, -50.0f), QVector2D(0.66f, 0.5f)}, // v8
        {QVector3D(-50.0f, -50.0f, -50.0f), QVector2D(1.0f, 0.5f)},  // v9
        {QVector3D( 50.0f,  50.0f, -50.0f), QVector2D(0.66f, 1.0f)}, // v10
        {QVector3D(-50.0f,  50.0f, -50.0f), QVector2D(1.0f, 1.0f)},  // v11

        {QVector3D(-50.0f, -50.0f, -50.0f), QVector2D(0.66f, 0.0f)}, // v12
        {QVector3D(-50.0f, -50.0f,  50.0f), QVector2D(1.0f, 0.0f)},  // v13
        {QVector3D(-50.0f,  50.0f, -50.0f), QVector2D(0.66f, 0.5f)}, // v14
        {QVector3D(-50.0f,  50.0f,  50.0f), QVector2D(1.0f, 0.5f)},  // v15

        {QVector3D(-50.0f, -50.0f, -50.0f), QVector2D(0.33f, 0.0f)}, // v16
        {QVector3D( 50.0f, -50.0f, -50.0f), QVector2D(0.66f, 0.0f)}, // v17
        {QVector3D(-50.0f, -50.0f,  50.0f), QVector2D(0.33f, 0.5f)}, // v18
        {QVector3D( 50.0f, -50.0f,  50.0f), QVector2D(0.66f, 0.5f)}, // v19

        {QVector3D(-50.0f,  50.0f,  50.0f), QVector2D(0.33f, 0.5f)}, // v20
        {QVector3D( 50.0f,  50.0f,  50.0f), QVector2D(0.66f, 0.5f)}, // v21
        {QVector3D(-50.0f,  50.0f, -50.0f), QVector2D(0.33f, 1.0f)}, // v22
        {QVector3D( 50.0f,  50.0f, -50.0f), QVector2D(0.66f, 1.0f)}  // v23
    };

    GLushort indices[] = {
         0,  1,  2,  3,  3,     // Face 0 - triangle strip ( v0,  v1,  v2,  v3)
         4,  4,  5,  6,  7,  7, // Face 1 - triangle strip ( v4,  v5,  v6,  v7)
         8,  8,  9, 10, 11, 11, // Face 2 - triangle strip ( v8,  v9, v10, v11)
        12, 12, 13, 14, 15, 15, // Face 3 - triangle strip (v12, v13, v14, v15)
        16, 16, 17, 18, 19, 19, // Face 4 - triangle strip (v16, v17, v18, v19)
        20, 20, 21, 22, 23      // Face 5 - triangle strip (v20, v21, v22, v23)
    };

    arrayBuf->bind();
    arrayBuf->allocate(vertices, 24 * sizeof(VertexData));

    indexBuf->bind();
    indexBuf->allocate(indices, 34 * sizeof(GLushort));
}

void dtkVrSimpleRendererPrivate::draw(void)
{
    arrayBuf->bind();
    indexBuf->bind();

    quintptr offset = 0;

    int vertexLocation = program.attributeLocation("a_position");
    program.enableAttributeArray(vertexLocation);
    program.setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    offset += sizeof(QVector3D);

    int texcoordLocation = program.attributeLocation("a_texcoord");
    program.enableAttributeArray(texcoordLocation);
    program.setAttributeBuffer(texcoordLocation, GL_FLOAT, offset, 2, sizeof(VertexData));

    glDrawElements(GL_TRIANGLE_STRIP, 34, GL_UNSIGNED_SHORT, nullptr);
}

void dtkVrSimpleRenderer::uninitializeGL(void)
{
     dtkVrRenderer::uninitializeGL();
}

void dtkVrSimpleRenderer::drawGL(void)
{
    d->texture->bind();
   
    d->program.bind();
    // d->program.setUniformValue("mvp_matrix", this->dtkVrRenderer::projection() * this->dtkVrRenderer::view() * this->dtkVrRenderer::model());
    d->program.setUniformValue("mvp_matrix", this->dtkVrRenderer::projection() * this->dtkVrRenderer::view() * this->dtkVrRenderer::model());
    d->program.setUniformValue("texture", 0);
    d->draw();
    d->program.release();

#if !defined(Q_OS_MAC)
    const QList<QOpenGLDebugMessage> messages = d->debugger->loggedMessages();

    for (const QOpenGLDebugMessage &message : messages)
        qDebug() << message;
#endif
}

void dtkVrSimpleRenderer::resizeGL(int width, int height)
{
     dtkVrRenderer::resizeGL(width, height);
}

void dtkVrSimpleRenderer::mouseMoveEvent(QMouseEvent *event)
{
    switch (event->button())
    {
    case Qt::LeftButton:
        break;

    case Qt::RightButton:
        break;

    default:
        break;
    }
}

void dtkVrSimpleRenderer::mousePressEvent(QMouseEvent *event)
{
    switch (event->button())
    {
    case Qt::LeftButton:
        break;

    case Qt::RightButton:
        break;

    default:
        break;
    }
}

void dtkVrSimpleRenderer::mouseReleaseEvent(QMouseEvent *event)
{
    switch (event->button())
    {
    case Qt::LeftButton:
        break;

    case Qt::RightButton:
        break;

    default:
        break;
    }
}

void dtkVrSimpleRenderer::keyPressEvent(QKeyEvent *event)
{
    switch( event->key()) {
    default:
        break;
    }
}

//
// dtkVrSimpleRenderer.cpp ends here
