// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkVrTrackerVrpn.h"

class dtkVrTrackerVrpnViconPrivate;

class dtkVrTrackerVrpnVicon : public dtkVrTrackerVrpn
{
    Q_OBJECT

public:
     dtkVrTrackerVrpnVicon(void);
    ~dtkVrTrackerVrpnVicon(void);

public:
      QVector3D position(void);
    QQuaternion orientation(void);

private:
    dtkVrTrackerVrpnViconPrivate *d;
};

//
// dtkVrTrackerVrpnVicon.h ends here
