// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkVrTracker.h"

class dtkVrTrackerCameraPrivate;

class dtkVrTrackerCamera : public dtkVrTracker
{
    Q_OBJECT

public:
     dtkVrTrackerCamera(void);
    ~dtkVrTrackerCamera(void);

public:
    void   initialize(void) override;
    void uninitialize(void) override;

public:
      QVector3D position(void)    override;
    QQuaternion orientation(void) override;

private:
    dtkVrTrackerCameraPrivate *d;
};

//
// dtkVrTrackerCamera.h ends here
