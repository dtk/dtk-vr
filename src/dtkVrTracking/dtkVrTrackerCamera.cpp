// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrTrackerCamera.h"

#include <dtkVr/dtkVrConfig.h>

// /////////////////////////////////////////////////////////////////////////////
// Preamble: Like it or not, oh my ....
// /////////////////////////////////////////////////////////////////////////////

#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

#include <iostream>

using namespace cv;

CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;

// ///////////////////////////////////////////////////////////////////
// dtkVrTrackerCameraPrivate
// ///////////////////////////////////////////////////////////////////

class dtkVrTrackerCameraPrivate : public QThread
{
public:
    void run(void);
    void stop(void);

public:
    bool running;

public:
    void    positionHandler(void);
    void orientationHandler(void);

public:
    static dtkVrTrackerCameraPrivate *self;

public:
    VideoCapture *video;

public:
      QVector3D position;
    QQuaternion orientation;
};

void dtkVrTrackerCameraPrivate::run(void)
{
    while(this->running) {
        this->positionHandler();
        this->orientationHandler();
        msleep(10);
    }
}

void dtkVrTrackerCameraPrivate::stop(void)
{
    this->running = false;
    this->terminate();
    this->wait();
}

void dtkVrTrackerCameraPrivate::positionHandler(void)
{
    Mat frame;

    video->read(frame);

    Mat frame_gray;

    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);

    equalizeHist(frame_gray, frame_gray);

    std::vector<Rect> faces;

    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(200, 200));

    Point p;

    if(faces.size())
        p = Point(faces[0].x + faces[0].width/2, faces[0].y + faces[0].height/2);
    else
        return;

    self->position = QVector3D(-p.x, -p.y, 0);
    self->position += QVector3D(350*2, 300*2, 0);

    // qDebug() << Q_FUNC_INFO << self->position;
}

void dtkVrTrackerCameraPrivate::orientationHandler(void)
{
    // ...

    // self->orientation = ...;
    // self->orientation.normalize();
}

dtkVrTrackerCameraPrivate *dtkVrTrackerCameraPrivate::self = NULL;

// ///////////////////////////////////////////////////////////////////
// dtkVrTrackerCamera
// ///////////////////////////////////////////////////////////////////

dtkVrTrackerCamera::dtkVrTrackerCamera(void) : dtkVrTracker(), d(new dtkVrTrackerCameraPrivate)
{
    d->self = d;

    d->video = new VideoCapture(0);

    if(!d->video->isOpened()) {
        qDebug() << Q_FUNC_INFO << "Could not open camera";
        exit(0);
    }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    QString eyes = DTKVR_SOURCE_DIR + "/src/dtkVrTracking/dtkVrTrackerCamera.eyes.xml";
    QString face = DTKVR_SOURCE_DIR + "/src/dtkVrTracking/dtkVrTrackerCamera.face.xml";

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    String face_cascade_name = face.toStdString();
    String eyes_cascade_name = eyes.toStdString();

// /////////////////////////////////////////////////////////////////////////////

    if(!face_cascade.load(face_cascade_name)) {
        std::cerr << "--(!)Error loading face cascade\n" << std::endl;
        return;
    };

    if(!eyes_cascade.load(eyes_cascade_name)) {
        std::cerr << "--(!)Error loading eyes cascade\n" << std::endl;
        return;
    };
}

dtkVrTrackerCamera::~dtkVrTrackerCamera(void)
{
    delete d;
}

void dtkVrTrackerCamera::initialize(void)
{
    d->start();
}

void dtkVrTrackerCamera::uninitialize(void)
{
    d->stop();
}

QVector3D dtkVrTrackerCamera::position(void)
{
    return d->position;
}

QQuaternion dtkVrTrackerCamera::orientation(void)
{
    return d->orientation;
}

//
// dtkVrTrackerCamera.cpp ends here
