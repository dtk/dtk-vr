// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkVrTracker.h"

#include <QtCore>

class dtkVrTrackerVrpnPrivate;

class dtkVrTrackerVrpn : public dtkVrTracker
{
    Q_OBJECT

public:
     dtkVrTrackerVrpn(void);
    ~dtkVrTrackerVrpn(void);

public:
    void   initialize(void);
    void uninitialize(void);

public:
    void setUrl(const QString& url);
    QString url(void);

public:
    virtual   QVector3D position(void);
    virtual QQuaternion orientation(void);

public:
    typedef void (*dtkVrTrackerVrpnPositionHandler)(float, float, float);
    void registerPositionHandler(int id, dtkVrTrackerVrpn::dtkVrTrackerVrpnPositionHandler handler);

public:
    typedef void (*dtkVrTrackerVrpnOrientationHandler)(float, float, float, float);
    void registerOrientationHandler(int id, dtkVrTrackerVrpn::dtkVrTrackerVrpnOrientationHandler handler);

public:
    typedef void (*dtkVrTrackerVrpnAnalogHandler)(QList<float>);
    void registerAnalogHandler(int id, dtkVrTrackerVrpn::dtkVrTrackerVrpnAnalogHandler handler);

protected slots:
    void startConnection(const QString& server);
    void  stopConnection(void);

protected:
    void runPositionHandlers(int id, float x, float y, float z);
    void runOrientationHandlers(int id, float q0, float q1, float q2, float q3);
    void runAnalogHandlers(int id, QList<float> values);

private:
    friend class dtkVrTrackerVrpnPrivate; dtkVrTrackerVrpnPrivate *d;
};

//
// dtkVrTrackerVrpn.h ends here
