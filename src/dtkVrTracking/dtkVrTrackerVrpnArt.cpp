// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrTrackerVrpnArt.h"

// ///////////////////////////////////////////////////////////////////
// dtkVrTrackerVrpnArtPrivate
// ///////////////////////////////////////////////////////////////////

class dtkVrTrackerVrpnArtPrivate
{
public:
    static void    positionHandler(float x, float y, float z);
    static void orientationHandler(float w, float x, float y, float z);

public:
    static dtkVrTrackerVrpnArtPrivate *self;

public:
      QVector3D position;
    QQuaternion orientation;

};

void dtkVrTrackerVrpnArtPrivate::positionHandler(float x, float y, float z)
{
    const qreal factor = 100;

    self->position.setX(+x*factor);
    self->position.setY(+z*factor);
    self->position.setZ(-y*factor);
}

void dtkVrTrackerVrpnArtPrivate::orientationHandler(float w, float x, float y, float z)
{
    QQuaternion q1 = QQuaternion::fromAxisAndAngle(QVector3D(1.0, 0.0, 0.0), -90);
    QQuaternion q2(w, x, y, z);

    self->orientation = q1*q2;
}

dtkVrTrackerVrpnArtPrivate *dtkVrTrackerVrpnArtPrivate::self = NULL;

// ///////////////////////////////////////////////////////////////////
// dtkVrTrackerVrpnArt
// ///////////////////////////////////////////////////////////////////

dtkVrTrackerVrpnArt::dtkVrTrackerVrpnArt(void) : dtkVrTrackerVrpn(), d(new dtkVrTrackerVrpnArtPrivate)
{
    d->self = d;

    // TODO:
    // instead of void, use the id of the tracker to initialize the proper handler
    // Here we always use 0, which is the id of the head tracker
    this->registerPositionHandler(0,&dtkVrTrackerVrpnArtPrivate::positionHandler);
    this->registerOrientationHandler(0,&dtkVrTrackerVrpnArtPrivate::orientationHandler);
}

dtkVrTrackerVrpnArt::~dtkVrTrackerVrpnArt(void)
{
    delete d;
}

QVector3D dtkVrTrackerVrpnArt::position(void)
{
    return d->position;
}

QQuaternion dtkVrTrackerVrpnArt::orientation(void)
{
    return d->orientation;
}

//
// dtkVrTrackerVrpnArt.cpp ends here
