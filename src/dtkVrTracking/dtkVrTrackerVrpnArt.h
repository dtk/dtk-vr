// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include "dtkVrTrackerVrpn.h"

class dtkVrTrackerVrpnArtPrivate;

class dtkVrTrackerVrpnArt : public dtkVrTrackerVrpn
{
    Q_OBJECT

public:
     dtkVrTrackerVrpnArt(void);
    ~dtkVrTrackerVrpnArt(void);

public:
      QVector3D position(void);
    QQuaternion orientation(void);

private:
    dtkVrTrackerVrpnArtPrivate *d;
};

//
// dtkVrTrackerVrpnArt.h ends here
