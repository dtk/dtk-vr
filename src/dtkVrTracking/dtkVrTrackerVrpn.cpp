// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrTrackerVrpn.h"

#include <vrpn_Shared.h>
#include <vrpn_Button.h>
#include <vrpn_Analog.h>
#include <vrpn_Tracker.h>
#include <vrpn_FileConnection.h>

// /////////////////////////////////////////////////////////////////
// vrpn callbacks (Definition at EOF.)
// /////////////////////////////////////////////////////////////////

void VRPN_CALLBACK vrpn_tracker_handle_tracker(void *data, const vrpn_TRACKERCB callback);
void VRPN_CALLBACK vrpn_tracker_handle_analog(void *data, const vrpn_ANALOGCB callback);

// /////////////////////////////////////////////////////////////////
// dtkVrTrackerVrpnPrivate
// /////////////////////////////////////////////////////////////////

#define MAX_TRACKER_ID 16

class dtkVrTrackerVrpnPrivate : public QThread
{
public:
    void run(void);
    void stop(void);

    void handle_analog(const vrpn_ANALOGCB callback);
    void handle_tracker(const vrpn_TRACKERCB callback);

public:
    bool running;

public:
    vrpn_Tracker_Remote *tracker;
    vrpn_Analog_Remote  *analog;

public:
    QList<dtkVrTrackerVrpn::dtkVrTrackerVrpnPositionHandler> position_handlers[MAX_TRACKER_ID];
    QList<dtkVrTrackerVrpn::dtkVrTrackerVrpnOrientationHandler> orientation_handlers[MAX_TRACKER_ID];
    QList<dtkVrTrackerVrpn::dtkVrTrackerVrpnAnalogHandler> analog_handlers[MAX_TRACKER_ID];

public:
    QString url;

public:
    QVector3D   position;
    QQuaternion orientation;

public:
    dtkVrTrackerVrpn *q;
};

void dtkVrTrackerVrpnPrivate::run(void)
{
    vrpn_FILE_CONNECTIONS_SHOULD_PRELOAD = false;
    vrpn_FILE_CONNECTIONS_SHOULD_ACCUMULATE = false;

    /*
    qDebug() << "=== VRPN init:" <<  qPrintable(url.toString());
    this->tracker = new vrpn_Tracker_Remote(qPrintable(url.toString()));
    this->analog = new vrpn_Analog_Remote(qPrintable(url.toString()));
    */
    qDebug() << "=== VRPN init:" <<  qPrintable(url);
    this->tracker = new vrpn_Tracker_Remote(qPrintable(url));
    this->analog = new vrpn_Analog_Remote(qPrintable(url));

    if (!this->tracker) {
        qDebug() << "Error connecting vrpn tracker to server" << url;
        return;
    }

    if (!this->analog) {
        qDebug() << "Error connecting vrpn analog to server" << url;
        return;
    }

    this->tracker->register_change_handler(this, vrpn_tracker_handle_tracker);
    this->analog->register_change_handler(this, vrpn_tracker_handle_analog);

    while(this->running) {
        this->tracker->mainloop();
        this->analog->mainloop();
        vrpn_SleepMsecs(1);
    }
}

void dtkVrTrackerVrpnPrivate::stop(void)
{
    this->running = false;
    this->terminate();
    this->wait();

    delete this->tracker;
}

void dtkVrTrackerVrpnPrivate::handle_tracker(const vrpn_TRACKERCB callback)
{
    q->runPositionHandlers(callback.sensor, callback.pos[0], callback.pos[1], callback.pos[2]);
    q->runOrientationHandlers(callback.sensor, callback.quat[0], callback.quat[1], callback.quat[2], callback.quat[3]);
}

void dtkVrTrackerVrpnPrivate::handle_analog(const vrpn_ANALOGCB callback)
{
    QList<float> values;

    for(int i = 0; i < callback.num_channel; i++)
        values << callback.channel[i];

    for(int i=0; i<MAX_TRACKER_ID; i++)
        q->runAnalogHandlers(i, values);
}


// /////////////////////////////////////////////////////////////////
// dtkVrTrackerVrpn
// /////////////////////////////////////////////////////////////////

dtkVrTrackerVrpn::dtkVrTrackerVrpn(void) : dtkVrTracker(), d(new dtkVrTrackerVrpnPrivate)
{
    d->q = this;
    d->running = false;
}

dtkVrTrackerVrpn::~dtkVrTrackerVrpn(void)
{
    delete d;

    d = NULL;
}

void dtkVrTrackerVrpn::initialize(void)
{
    this->startConnection(d->url);
}

void dtkVrTrackerVrpn::uninitialize(void)
{
    this->stopConnection();
}

void dtkVrTrackerVrpn::setUrl(const QString& url)
{
    d->url = url;
}

QString dtkVrTrackerVrpn::url(void)
{
  return(d->url);
}

QVector3D dtkVrTrackerVrpn::position(void)
{
    return QVector3D();
}

QQuaternion dtkVrTrackerVrpn::orientation(void)
{
    return QQuaternion();
}

void dtkVrTrackerVrpn::registerPositionHandler(int id, dtkVrTrackerVrpn::dtkVrTrackerVrpnPositionHandler handler)
{
    if ( ( id >= 0 ) &&
         ( id < MAX_TRACKER_ID )) {
        d->position_handlers[id] << handler;
    }
    else {
        qDebug() << Q_FUNC_INFO << "Bad VRPN dtkVrTracker id";
    }
}

void dtkVrTrackerVrpn::registerOrientationHandler(int id, dtkVrTrackerVrpn::dtkVrTrackerVrpnOrientationHandler handler)
{
    if ( ( id >= 0 ) &&
         ( id < MAX_TRACKER_ID )) {
        d->orientation_handlers[id] << handler;
    }
    else {
        qDebug() << Q_FUNC_INFO << "Bad VRPN dtkVrTracker id";
    }
}

void dtkVrTrackerVrpn::registerAnalogHandler(int id, dtkVrTrackerVrpn::dtkVrTrackerVrpnAnalogHandler handler)
{
    if ( ( id >= 0 ) &&
         ( id < MAX_TRACKER_ID )) {
        d->analog_handlers[id] << handler;
    }
    else {
        qDebug() << Q_FUNC_INFO << "Bad VRPN dtkVrTracker id";
    }
}

void dtkVrTrackerVrpn::startConnection(const QString& server)
{
    d->running = true;
    d->url = server;
    d->start();
}

void dtkVrTrackerVrpn::stopConnection(void)
{
    d->stop();
}

void dtkVrTrackerVrpn::runPositionHandlers(int id, float x, float y, float z)
{
    if ( ( id >= 0 ) &&
         ( id < MAX_TRACKER_ID )) {
        foreach(dtkVrTrackerVrpnPositionHandler handler, d->position_handlers[id])
            (handler)(x, y, z);
    }
    else {
        qDebug() << Q_FUNC_INFO << "Bad VRPN dtkVrTracker id";
    }
}

void dtkVrTrackerVrpn::runOrientationHandlers(int id, float q0, float q1, float q2, float q3)
{
    if ( ( id >= 0 ) &&
         ( id < MAX_TRACKER_ID )) {
        foreach(dtkVrTrackerVrpnOrientationHandler handler, d->orientation_handlers[id])
            (handler)(q0, q1, q2, q3);
    }
    else {
        qDebug() << Q_FUNC_INFO << "Bad VRPN dtkVrTracker id";
    }
}

void dtkVrTrackerVrpn::runAnalogHandlers(int id, QList<float> values)
{
    if ( ( id >= 0 ) &&
         ( id < MAX_TRACKER_ID )) {
        foreach(dtkVrTrackerVrpnAnalogHandler handler, d->analog_handlers[id])
            (handler)(values);
    }
    else {
        qDebug() << Q_FUNC_INFO << "Bad VRPN dtkVrTracker id";
    }
}

// /////////////////////////////////////////////////////////////////
// vrpn callbacks
// /////////////////////////////////////////////////////////////////

void VRPN_CALLBACK vrpn_tracker_handle_tracker(void *data, const vrpn_TRACKERCB callback)
{
    if(dtkVrTrackerVrpnPrivate *d = static_cast<dtkVrTrackerVrpnPrivate *>(data))
        d->handle_tracker(callback);
}

void VRPN_CALLBACK vrpn_tracker_handle_analog(void *data, const vrpn_ANALOGCB callback)
{
    if(dtkVrTrackerVrpnPrivate *d = static_cast<dtkVrTrackerVrpnPrivate *>(data))
        d->handle_analog(callback);
}

//
// dtkVrTrackerVrpn.cpp ends here
