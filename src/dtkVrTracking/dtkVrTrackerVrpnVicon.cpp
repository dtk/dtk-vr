// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrTrackerVrpnVicon.h"

// ///////////////////////////////////////////////////////////////////
// dtkVrTrackerVrpnViconPrivate
// ///////////////////////////////////////////////////////////////////

class dtkVrTrackerVrpnViconPrivate
{
public:
    static void    positionHandler(float x, float y, float z);
    static void orientationHandler(float w, float x, float y, float z);

public:
    static dtkVrTrackerVrpnViconPrivate *self;

public:
      QVector3D position;
    QQuaternion orientation;

};

void dtkVrTrackerVrpnViconPrivate::positionHandler(float x, float y, float z)
{
    const qreal factor = 100.0;

    QVector3D position_received;

    position_received.setX(+x*factor);
    position_received.setY(+y*factor);
    position_received.setZ(+z*factor);

    self->position = position_received;
}

void dtkVrTrackerVrpnViconPrivate::orientationHandler(float w, float x, float y, float z)
{
    QQuaternion rot1 = QQuaternion::fromAxisAndAngle(QVector3D(0.0, 1.0, 0.0), 180.0).normalized();
    QQuaternion rot2 = QQuaternion::fromAxisAndAngle(QVector3D(0.0, 0.0, 1.0), 180.0).normalized();
    QQuaternion q(w, -z, y, -x); q.normalize();

    self->orientation = rot1 * rot2 * q;
    self->orientation.normalize();
}

dtkVrTrackerVrpnViconPrivate *dtkVrTrackerVrpnViconPrivate::self = NULL;

// ///////////////////////////////////////////////////////////////////
// dtkVrTrackerVrpnVicon
// ///////////////////////////////////////////////////////////////////

dtkVrTrackerVrpnVicon::dtkVrTrackerVrpnVicon(void) : dtkVrTrackerVrpn(), d(new dtkVrTrackerVrpnViconPrivate)
{
    d->self = d;

// /////////////////////////////////////////////////////////////////////////////
// TODO:
// Instead of void, use the id of the tracker to initialize the proper handler
// Here we always use 0, which is the id of the head tracker
// /////////////////////////////////////////////////////////////////////////////

    this->registerPositionHandler(0, &dtkVrTrackerVrpnViconPrivate::positionHandler);
    this->registerOrientationHandler(0, &dtkVrTrackerVrpnViconPrivate::orientationHandler);
}

dtkVrTrackerVrpnVicon::~dtkVrTrackerVrpnVicon(void)
{
    delete d;
}

QVector3D dtkVrTrackerVrpnVicon::position(void)
{
    return d->position;
}

QQuaternion dtkVrTrackerVrpnVicon::orientation(void)
{
    return d->orientation;
}

//
// dtkVrTrackerVrpnVicon.cpp ends here
