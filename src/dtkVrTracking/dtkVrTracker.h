// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtMath>
#include <QtNetwork>

class dtkVrTrackerPrivate;

class dtkVrTracker : public QObject
{
    Q_OBJECT

public:
     dtkVrTracker(void);
    ~dtkVrTracker(void);

public:
    virtual void   initialize(void);
    virtual void uninitialize(void);

public:
    virtual   QVector3D position(void);
    virtual QQuaternion orientation(void);

public:
    virtual bool  cummulative(void);
    virtual float sensitivity(void);

public:
    virtual void setUrl(const QString& url);
    virtual QString url(void);

public:
    virtual void setCummulative(bool  cummulative);
    virtual void setSensitivity(float sensitivity);

private:
    dtkVrTrackerPrivate *d;
};

//
// dtkVrTracker.h ends here
