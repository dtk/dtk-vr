// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrTracker.h"

class dtkVrTrackerPrivate
{
public:
    bool   cummulative;
    float  sensitivity;
};

dtkVrTracker::dtkVrTracker(void) : QObject(), d(new dtkVrTrackerPrivate)
{
    d->cummulative = false;
    d->sensitivity = 1.0;
}

dtkVrTracker::~dtkVrTracker(void)
{
    delete d;

    d = NULL;
}

void dtkVrTracker::initialize(void)
{

}

void dtkVrTracker::uninitialize(void)
{

}

void dtkVrTracker::setUrl(const QString& url)
{
    Q_UNUSED(url);
}

QString dtkVrTracker::url(void)
{
  return QString();
}

QVector3D dtkVrTracker::position(void)
{
    return QVector3D();
}

QQuaternion dtkVrTracker::orientation(void)
{
    return QQuaternion();
}

bool dtkVrTracker::cummulative(void)
{
    return d->cummulative;
}

void dtkVrTracker::setCummulative(bool cummulative)
{
    d->cummulative = cummulative;
}

float dtkVrTracker::sensitivity(void)
{
    return d->sensitivity;
}

void dtkVrTracker::setSensitivity(float sensitivity)
{
    d->sensitivity = sensitivity;
}

//
// dtkVrTracker.cpp ends here
