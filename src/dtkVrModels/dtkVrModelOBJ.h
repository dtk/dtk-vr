// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkVrModel.h"

class dtkVrModelOBJ : public dtkVrModel
{
public:
     dtkVrModelOBJ(void);
    ~dtkVrModelOBJ(void);

public:
    void load(const QString&) override;
    void setup(GLuint) override;
    void render(void) override;

private:
    class dtkVrModelOBJPrivate *d;
};

// 
// dtkVrModelOBJ.h ends here
