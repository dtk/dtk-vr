// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtOpenGL>

// /////////////////////////////////////////////////////////////////////////////
// TODO: Check this on macOS
// /////////////////////////////////////////////////////////////////////////////
#if !defined(Q_OS_MAC)
class dtkVrModel : public QObject, protected QOpenGLExtraFunctions
#else
class dtkVrModel : public QObject, protected QOpenGLFunctions
#endif
// /////////////////////////////////////////////////////////////////////////////
{
public:
             dtkVrModel(void) { this->initializeOpenGLFunctions(); }
    virtual ~dtkVrModel(void) = default;

public:
    virtual void load(const QString&) = 0;
    virtual void setup(GLuint) = 0;
    virtual void render(void) = 0;
};

//
// dtkVrModel.h ends here
