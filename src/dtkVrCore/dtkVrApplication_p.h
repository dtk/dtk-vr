// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkVrApplication.h"

#include <QtCore>

class dtkVrRenderer;
class dtkVrTracker;

class dtkVrApplicationPrivate
{
public:
    QList<dtkVrTracker *> trackers;

public:
    dtkVrRenderer *renderer;

public:
    dtkVrApplication::StereoMode stereo = dtkVrApplication::Mono;

public:
    QCommandLineParser *parser;

public:
    QHash<QString, QVariant> config;
    QHash<QString, QMetaType::Type> syntax;
};

//
// dtkVrApplication_p.h ends here
