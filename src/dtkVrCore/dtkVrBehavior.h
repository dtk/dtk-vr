// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkVrRenderer;
class dtkVrTracker;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkVrBehavior
{
public:
    typedef void (*behaviorHandler)(dtkVrTracker*, dtkVrRenderer *);

public:
     dtkVrBehavior(QString, dtkVrBehavior::behaviorHandler);
    ~dtkVrBehavior(void) ;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkVrBehaviors : public QObject
{
    Q_OBJECT

public:
    static dtkVrBehaviors *instance(void);

private:
     dtkVrBehaviors(void);
    ~dtkVrBehaviors(void);

public:
    bool invoque(dtkVrTracker *, dtkVrRenderer *, bool);

public:
    bool associate(dtkVrTracker *, QString);

private:
    bool record(QString, dtkVrBehavior::behaviorHandler);

private:
     class dtkVrBehaviorsPrivate *d;
    static dtkVrBehaviors *s_instance;

private:
    friend class dtkVrBehavior;
};

//
// dtkVrBehavior.h ends here
