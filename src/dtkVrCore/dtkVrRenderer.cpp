// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrApplication.h"
#include "dtkVrBehavior.h"
#include "dtkVrRenderer.h"

#include <QtDebug>

// /////////////////////////////////////////////////////////////////////////////
// NOTE: In progress
// /////////////////////////////////////////////////////////////////////////////

class dtkVrRendererPrivate
{
public:
    int display_width;
    int display_height;

public:
    QVector3D scene_scale;
    QVector3D scene_position;
    QQuaternion scene_orientation;

public:
    QVector3D   scene_offset_scale;
    QVector3D   scene_offset_position;
    QQuaternion scene_offset_orientation;

public:
    bool drawing_left_eye = false;

public:
    QVector3D screen_upper_left;
    QVector3D screen_lower_left;
    QVector3D screen_lower_right;

    QVector3D vr;
    QVector3D vu;
    QVector3D vn;

    QVector3D   head_position;
    QQuaternion head_orientation;
    double      interocular_distance;

    QVector3D left_eye_delta;
    QVector3D right_eye_delta;

    QVector3D calibration_offset = QVector3D( +0.0, +1.0, -3.0);

    double mono_frustum[6];

    double stereo_left_frustum[6];
    double stereo_right_frustum[6];
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkVrRenderer::dtkVrRenderer(void)
{
    d = new dtkVrRendererPrivate;

    d->display_width  = dtkVrApp->screens().first()->availableSize().width();
    d->display_height = dtkVrApp->screens().first()->availableSize().height();

    d->scene_scale = QVector3D(1.0, 1.0, 1.0);
    d->scene_offset_scale = QVector3D(1.0, 1.0, 1.0);

    d->interocular_distance = 6.5 * 20;

    d->left_eye_delta  = QVector3D(-d->interocular_distance / 2.0, 0.0, -3.0) + d->calibration_offset;
    d->right_eye_delta = QVector3D(+d->interocular_distance / 2.0, 0.0, -3.0) + d->calibration_offset;
}

dtkVrRenderer::~dtkVrRenderer(void)
{
    delete d;
}

double dtkVrRenderer::interocularDistance(void)
{
    return d->interocular_distance;
}

QVector3D dtkVrRenderer::scenePosition(void)
{
    return d->scene_position;
}

QQuaternion dtkVrRenderer::sceneOrientation(void)
{
    return d->scene_orientation;
}

QVector3D dtkVrRenderer::sceneScale(void)
{
    return d->scene_scale;
}

void dtkVrRenderer::setHeadPosition(QVector3D position)
{
    d->head_position = position;
}

void dtkVrRenderer::setHeadOrientation(QQuaternion orientation)
{
    d->head_orientation = orientation;
    d->head_orientation.normalize();
}

QVector3D dtkVrRenderer::headPosition(void)
{
    return d->head_position;
}

QQuaternion dtkVrRenderer::headOrientation(void)
{
    return d->head_orientation;
}

void dtkVrRenderer::setInterocularDistance(double distance)
{
    if (distance > 0.0)
        d->interocular_distance = distance;

    d->left_eye_delta  = QVector3D( - d->interocular_distance / 2.0, 0.0, -3.0) + d->calibration_offset;
    d->right_eye_delta = QVector3D(   d->interocular_distance / 2.0, 0.0, -3.0) + d->calibration_offset;
}

void dtkVrRenderer::setScenePosition(QVector3D position)
{
    d->scene_position = position;
}

void dtkVrRenderer::setSceneOrientation(QQuaternion orientation)
{
   d->scene_orientation = orientation;
   d->scene_orientation.normalize();
}

void dtkVrRenderer::setSceneScale(QVector3D scale)
{
    d->scene_scale = scale;
}

bool dtkVrRenderer::drawingLeftEye(void)
{
    if(dtkVrApp->stereo() == dtkVrApplication::Mono)
        return true;

    return d->drawing_left_eye ? true : false;
}

bool dtkVrRenderer::drawingRightEye(void)
{
    if(dtkVrApp->stereo() == dtkVrApplication::Mono)
        return true;

    return d->drawing_left_eye ? false : true;
}

QVector3D dtkVrRenderer::sceneOffsetPosition(void)
{
    return d->scene_offset_position;
}

QQuaternion dtkVrRenderer::sceneOffsetOrientation(void)
{
    return d->scene_offset_orientation;
}

QVector3D dtkVrRenderer::sceneOffsetScale(void)
{
    return d->scene_offset_scale;
}

void dtkVrRenderer::setSceneOffsetPosition(QVector3D position)
{
    d->scene_offset_position = position;
}

void dtkVrRenderer::setSceneOffsetOrientation(QQuaternion orientation)
{
    d->scene_offset_orientation = orientation ;
    d->scene_offset_orientation.normalize();
}

void dtkVrRenderer::setSceneOffsetScale(QVector3D scale)
{
    d->scene_offset_scale = scale;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void dtkVrRenderer::initializeGL(void)
{
    this->initializeOpenGLFunctions();

    glClearColor(0.1, 0.1, 0.1, 1.0);
}

void dtkVrRenderer::uninitializeGL(void)
{
   
}

void dtkVrRenderer::paintGL(void)
{
// /////////////////////////////////////////////////////////////////////////////
// Preprocessing
// /////////////////////////////////////////////////////////////////////////////

    d->screen_upper_left  = QVector3D(-151.1, 221,  -159.8);
    d->screen_lower_left  = QVector3D(-151.1,  51,  -159.8);
    d->screen_lower_right = QVector3D( 151.1,  51,  -159.8);

// /////////////////////////////////////////////////////////////////////////////

    d->drawing_left_eye = true;

    d->vr = (d->screen_lower_right - d->screen_lower_left).normalized();
    d->vu =  (d->screen_upper_left - d->screen_lower_left).normalized();
    d->vn = QVector3D::crossProduct(d->vr, d->vu);

    if(dtkVrApp->stereo() == dtkVrApplication::Mono) {

        QVector3D va = d->screen_lower_left  - d->head_position;
        QVector3D vb = d->screen_lower_right - d->head_position;
        QVector3D vc = d->screen_upper_left  - d->head_position;

        double D = -QVector3D::dotProduct(va, d->vn);

        d->mono_frustum[FRUSTUM_NEAR]   = 0.1;
        d->mono_frustum[FRUSTUM_FAR]    = 10000;
        d->mono_frustum[FRUSTUM_LEFT]   = QVector3D::dotProduct(d->vr, va) * d->mono_frustum[FRUSTUM_NEAR]/D;
        d->mono_frustum[FRUSTUM_RIGHT]  = QVector3D::dotProduct(d->vr, vb) * d->mono_frustum[FRUSTUM_NEAR]/D;
        d->mono_frustum[FRUSTUM_BOTTOM] = QVector3D::dotProduct(d->vu, va) * d->mono_frustum[FRUSTUM_NEAR]/D;
        d->mono_frustum[FRUSTUM_TOP]    = QVector3D::dotProduct(d->vu, vc) * d->mono_frustum[FRUSTUM_NEAR]/D;
    }

    if(dtkVrApp->stereo() == dtkVrApplication::Quad) {
   
        { // Left Eye

            QVector3D left_eye = d->head_position + d->head_orientation.rotatedVector( d->left_eye_delta);

            QVector3D va = d->screen_lower_left  - left_eye;
            QVector3D vb = d->screen_lower_right - left_eye;
            QVector3D vc = d->screen_upper_left  - left_eye;

            double D = -QVector3D::dotProduct(va, d->vn);

            d->stereo_left_frustum[FRUSTUM_NEAR]   = 0.1;
            d->stereo_left_frustum[FRUSTUM_FAR]    = 10000;
            d->stereo_left_frustum[FRUSTUM_LEFT]   = QVector3D::dotProduct(d->vr, va) * d->stereo_left_frustum[FRUSTUM_NEAR]/D;
            d->stereo_left_frustum[FRUSTUM_RIGHT]  = QVector3D::dotProduct(d->vr, vb) * d->stereo_left_frustum[FRUSTUM_NEAR]/D;
            d->stereo_left_frustum[FRUSTUM_BOTTOM] = QVector3D::dotProduct(d->vu, va) * d->stereo_left_frustum[FRUSTUM_NEAR]/D;
            d->stereo_left_frustum[FRUSTUM_TOP]    = QVector3D::dotProduct(d->vu, vc) * d->stereo_left_frustum[FRUSTUM_NEAR]/D;
        }

        { // Right Eye

            QVector3D right_eye = d->head_position + d->head_orientation.rotatedVector(d->right_eye_delta);

            QVector3D va = d->screen_lower_left  - right_eye;
            QVector3D vb = d->screen_lower_right - right_eye;
            QVector3D vc = d->screen_upper_left  - right_eye;

            double D = -QVector3D::dotProduct(va, d->vn);

            d->stereo_right_frustum[FRUSTUM_NEAR]   = 0.1;
            d->stereo_right_frustum[FRUSTUM_FAR]    = 10000;
            d->stereo_right_frustum[FRUSTUM_LEFT]   = QVector3D::dotProduct(d->vr, va) * d->stereo_right_frustum[FRUSTUM_NEAR]/D;
            d->stereo_right_frustum[FRUSTUM_RIGHT]  = QVector3D::dotProduct(d->vr, vb) * d->stereo_right_frustum[FRUSTUM_NEAR]/D;
            d->stereo_right_frustum[FRUSTUM_BOTTOM] = QVector3D::dotProduct(d->vu, va) * d->stereo_right_frustum[FRUSTUM_NEAR]/D;
            d->stereo_right_frustum[FRUSTUM_TOP]    = QVector3D::dotProduct(d->vu, vc) * d->stereo_right_frustum[FRUSTUM_NEAR]/D;
        }
    }

// /////////////////////////////////////////////////////////////////////////////
// Run the behaviors
// /////////////////////////////////////////////////////////////////////////////

    foreach(dtkVrTracker *tracker, dtkVrApp->trackers())
        dtkVrBehaviors::instance()->invoque(tracker, this, false);

// /////////////////////////////////////////////////////////////////////////////
// Rendering
// /////////////////////////////////////////////////////////////////////////////

    glViewport(0, 0, d->display_width, d->display_height);

// /////////////////////////////////////////////////////////////////////////////
// Monoscopic rendering
// /////////////////////////////////////////////////////////////////////////////

    if(dtkVrApp->stereo() == dtkVrApplication::Mono) {

        // glMatrixMode(GL_PROJECTION);

        // glDrawBuffer(GL_BACK); TODO: Here we go: glDrawBuffer is LEGACY hence DEPRECATED

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // glLoadIdentity();

        // qDebug() << Q_FUNC_INFO << "Frustum:";

        // qDebug()
        //     << d->mono_frustum[FRUSTUM_LEFT]
        //     << d->mono_frustum[FRUSTUM_RIGHT]
        //     << d->mono_frustum[FRUSTUM_BOTTOM]
        //     << d->mono_frustum[FRUSTUM_TOP]
        //     << d->mono_frustum[FRUSTUM_NEAR]
        //     << d->mono_frustum[FRUSTUM_FAR];

        // glFrustum(d->mono_frustum[FRUSTUM_LEFT],
        //           d->mono_frustum[FRUSTUM_RIGHT],
        //           d->mono_frustum[FRUSTUM_BOTTOM],
        //           d->mono_frustum[FRUSTUM_TOP],
        //           d->mono_frustum[FRUSTUM_NEAR],
        //           d->mono_frustum[FRUSTUM_FAR]);

        // qDebug() << Q_FUNC_INFO << "Projection:";

        // qDebug() << d->projection_matrix[ 0] << d->projection_matrix[ 1] << d->projection_matrix[ 2] << d->projection_matrix[ 3];
        // qDebug() << d->projection_matrix[ 4] << d->projection_matrix[ 5] << d->projection_matrix[ 6] << d->projection_matrix[ 7];
        // qDebug() << d->projection_matrix[ 8] << d->projection_matrix[ 9] << d->projection_matrix[10] << d->projection_matrix[11];
        // qDebug() << d->projection_matrix[12] << d->projection_matrix[13] << d->projection_matrix[14] << d->projection_matrix[15];

        // glMultMatrixf(d->projection_matrix);

        // qDebug() << Q_FUNC_INFO << "Head position" << d->head_position;

        // glTranslatef(-d->head_position.x(), -d->head_position.y(), -d->head_position.z());

        // glMatrixMode(GL_MODELVIEW);

        this->drawGL();
    }

// /////////////////////////////////////////////////////////////////////////////
// Quad Buffer Stereoscopic rendering
// /////////////////////////////////////////////////////////////////////////////

    if(dtkVrApp->stereo() == dtkVrApplication::Quad) {
   
        { // Left Eye

            d->drawing_left_eye = true;

            // glMatrixMode(GL_PROJECTION);

            // GLenum err = GL_NO_ERROR;
            // while((err = glGetError()) != GL_NO_ERROR);

            // glDrawBuffer(GL_BACK_LEFT); // TODO: Here we go: glDrawBuffer is LEGACY hence DEPRECATED

            // while((err = glGetError()) != GL_NO_ERROR)
            //     qDebug() << "GL_BACK_LEFT error" << err;

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // glLoadIdentity();

            // glFrustum(d->stereo_left_frustum[FRUSTUM_LEFT],
            //           d->stereo_left_frustum[FRUSTUM_RIGHT],
            //           d->stereo_left_frustum[FRUSTUM_BOTTOM],
            //           d->stereo_left_frustum[FRUSTUM_TOP],
            //           d->stereo_left_frustum[FRUSTUM_NEAR],
            //           d->stereo_left_frustum[FRUSTUM_FAR]);

            // QVector3D left_eye = d->head_position + d->head_orientation.rotatedVector(d->left_eye_delta);

            // // glMultMatrixf(d->projection_matrix);
            // // glTranslatef(-left_eye.x(), -left_eye.y(), -left_eye.z());
            // // glMatrixMode(GL_MODELVIEW);

            // // qDebug() << Q_FUNC_INFO << "L";

            this->drawGL();
        }

        { // Right Eye

            d->drawing_left_eye = false;

            // glMatrixMode(GL_PROJECTION);

            // GLenum err = GL_NO_ERROR;
            // while((err = glGetError()) != GL_NO_ERROR) ;

            // glDrawBuffer(GL_BACK_RIGHT); // TODO: Here we go: glDrawBuffer is LEGACY hence DEPRECATED

            // while((err = glGetError()) != GL_NO_ERROR)
            //     qDebug() << "GL_BACK_RIGHT error" << err;

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // glLoadIdentity();

            // glFrustum(d->stereo_right_frustum[FRUSTUM_LEFT],
            //           d->stereo_right_frustum[FRUSTUM_RIGHT],
            //           d->stereo_right_frustum[FRUSTUM_BOTTOM],
            //           d->stereo_right_frustum[FRUSTUM_TOP],
            //           d->stereo_right_frustum[FRUSTUM_NEAR],
            //           d->stereo_right_frustum[FRUSTUM_FAR]);

            // QVector3D right_eye = d->head_position + d->head_orientation.rotatedVector(d->right_eye_delta);

            // glMultMatrixf(d->projection_matrix);
            // glTranslatef(-right_eye.x(), -right_eye.y(), -right_eye.z());
            // glMatrixMode(GL_MODELVIEW);

            // qDebug() << Q_FUNC_INFO << "R";

            this->drawGL();
        }
    }
}

void dtkVrRenderer::resizeGL(int width, int height)
{
    d->display_width = width;
    d->display_height = height;

    if (height > 0) {
        const auto screen_ratio = double(width) / height;
        d->screen_upper_left.setY(d->screen_lower_left.y() + (d->screen_lower_right.x() - d->screen_lower_left.x()) / screen_ratio);
    }
}

void dtkVrRenderer::mouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void dtkVrRenderer::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void dtkVrRenderer::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void dtkVrRenderer::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
}

QVector3D dtkVrRenderer::screenUpperLeft(void)
{
    return d->screen_upper_left;
}

QVector3D dtkVrRenderer::screenLowerLeft(void)
{
    return d->screen_lower_left;
}

QVector3D dtkVrRenderer::screenLowerRight(void)
{
    return d->screen_lower_right;
}

QMatrix4x4 dtkVrRenderer::model(void)
{
    QMatrix4x4 m;
    m.translate(this->scenePosition());
    m.rotate(this->sceneOrientation());
    m.scale(this->sceneScale());

    return m;
}

QMatrix4x4 dtkVrRenderer::view(void)
{
    QMatrix4x4 m;

    return m;
}

QMatrix4x4 dtkVrRenderer::projection(void)
{
    QVector3D left_eye = d->head_position + d->head_orientation.rotatedVector(d->left_eye_delta);
    QVector3D righ_eye = d->head_position + d->head_orientation.rotatedVector(d->right_eye_delta);

    QMatrix4x4 m;

    if(dtkVrApp->stereo() == dtkVrApplication::Mono)
        m.frustum(d->mono_frustum[FRUSTUM_LEFT],
                  d->mono_frustum[FRUSTUM_RIGHT],
                  d->mono_frustum[FRUSTUM_BOTTOM],
                  d->mono_frustum[FRUSTUM_TOP],
                  d->mono_frustum[FRUSTUM_NEAR],
                  d->mono_frustum[FRUSTUM_FAR]);
    else if(d->drawing_left_eye)
        m.frustum(d->stereo_left_frustum[FRUSTUM_LEFT],
                  d->stereo_left_frustum[FRUSTUM_RIGHT],
                  d->stereo_left_frustum[FRUSTUM_BOTTOM],
                  d->stereo_left_frustum[FRUSTUM_TOP],
                  d->stereo_left_frustum[FRUSTUM_NEAR],
                  d->stereo_left_frustum[FRUSTUM_FAR]);

    else
        m.frustum(d->stereo_right_frustum[FRUSTUM_LEFT],
                  d->stereo_right_frustum[FRUSTUM_RIGHT],
                  d->stereo_right_frustum[FRUSTUM_BOTTOM],
                  d->stereo_right_frustum[FRUSTUM_TOP],
                  d->stereo_right_frustum[FRUSTUM_NEAR],
                  d->stereo_right_frustum[FRUSTUM_FAR]);

    if(dtkVrApp->stereo() == dtkVrApplication::Mono)
        m.translate(-1 * this->headPosition());
    else if(d->drawing_left_eye)
        m.translate(-1 * left_eye);
    else
        m.translate(-1 * righ_eye);

    return m;
}

//
// dtkVrRenderer.cpp ends here
