// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>

class dtkVrRenderer;
class dtkVrTracker;

class dtkVrApplication : public QGuiApplication
{
    Q_OBJECT

public:
    enum StereoMode {
        Mono     = 0,
        Anaglyph = 1,
        Quad     = 2
    };

public:
    dtkVrApplication(int& argc, char **argv);
   ~dtkVrApplication(void);

public:
           void initialize(void);
    static void initializeGL(int argc, char **argv);

public:
    StereoMode stereo(void);

public:
    QCommandLineParser *parser(void) const;

public:
    dtkVrRenderer *renderer(void);

public:
    QList<dtkVrTracker *> trackers(void);

public:
    QVariant config(QString);

public:
    bool isSet(QString);

private:
    class dtkVrApplicationPrivate *d;

private:
    friend class dtkVrView; // TODO: Beurk ... ou pas ...
};

#define dtkVrApp static_cast<dtkVrApplication *>(qGuiApp)

// 
// dtkVrApplication.h ends here
