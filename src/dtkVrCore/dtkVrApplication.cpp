// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrApplication.h"
#include "dtkVrApplication_p.h"
#include "dtkVrBehavior.h"

// #include <dtkLog>

#include <dtkVr/dtkVrConfig.h>

#include <dtkVrTracking/dtkVrTracker.h>
#if defined(DTKVR_HAS_OPENCV)
#include <dtkVrTracking/dtkVrTrackerCamera.h>
#endif
#if defined(DTKVR_HAS_VRPN)
#include <dtkVrTracking/dtkVrTrackerVrpn.h>
#include <dtkVrTracking/dtkVrTrackerVrpnArt.h>
#include <dtkVrTracking/dtkVrTrackerVrpnVicon.h>
#endif

dtkVrApplication::dtkVrApplication(int& argc, char **argv) : QGuiApplication(argc, argv)
{
    d = new dtkVrApplicationPrivate;

    d->syntax.insert("lower_left", QMetaType::QVector3D);
    d->syntax.insert("upper_left", QMetaType::QVector3D);
    d->syntax.insert("lower_right", QMetaType::QVector3D);
    d->syntax.insert("geometry", QMetaType::QRect);
    d->syntax.insert("stereo", QMetaType::QString);
    d->syntax.insert("position", QMetaType::QVector3D);
    d->syntax.insert("orientation", QMetaType::QQuaternion);
    d->syntax.insert("scale", QMetaType::QVector3D);
    d->syntax.insert("input", QMetaType::QString);
    d->syntax.insert("cummulative", QMetaType::Bool);
    d->syntax.insert("sensitivity", QMetaType::Double);
}

dtkVrApplication::~dtkVrApplication(void)
{
    foreach(dtkVrTracker *tracker, d->trackers)
        tracker->uninitialize();

    delete d;
}

QCommandLineParser *dtkVrApplication::parser(void) const
{
    return d->parser;
}

void dtkVrApplication::initialize(void)
{
    qputenv("QT_PLUGIN_PATH", "1");
    qputenv("LC_ALL", "C");

    QLocale::setDefault(QLocale::c());

#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    d->parser = new QCommandLineParser;
    d->parser->addHelpOption();
    d->parser->addVersionOption();
    d->parser->setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);

    QCommandLineOption envOption(QStringList() << "e" << "env" << "environment",
        dtkVrApplication::translate("main", "MANDATORY: specify at least one environment file (.ini), this argument is multi valued"),
        dtkVrApplication::translate("main", "file"));

    QCommandLineOption stereoOption(QStringList() << "s" << "stereo",
        dtkVrApplication::translate("main", "Activate stereo mode"),
        dtkVrApplication::translate("main", "mono | anaglyph | quad"));

    QCommandLineOption optOption(QStringList() << "o" << "option",
        dtkVrApplication::translate("main", "overload one or more options (ex: screen/front/stereo=mono), this argument is multi valued"),
        dtkVrApplication::translate("main", "option"));

    QCommandLineOption listOption(QStringList() << "l" << "list",
        dtkVrApplication::translate("main", "list all application options from .ini files and given arguments"));

    QCommandLineOption verboseOption("verbose",
        dtkVrApplication::translate("main", "verbose plugin initialization"));

    QString verbosity = "info";

    // QCommandLineOption loglevelOption("loglevel",
    //     dtkVrApplication::translate("main", "log level used by dtkLog (default is info)"), "trace|debug|info|warn|error|fatal", verbosity);

    // QCommandLineOption logfileOption("logfile",
    //     qPrintable(QString("log file used by dtkLog; default is: ").append(dtkLogPath(this))), "filename | console", dtkLogPath(this));

    d->parser->addOption(envOption);
    d->parser->addOption(stereoOption);
    d->parser->addOption(optOption);
    d->parser->addOption(listOption);
    d->parser->addOption(verboseOption);
    // d->parser->addOption(loglevelOption);
    // d->parser->addOption(logfileOption);
    d->parser->process(*this);

    // if (d->parser->isSet(logfileOption)) {

    //     if (d->parser->value(logfileOption) == "console")
    //         dtkLogger::instance().attachConsole();
    //     else
    //         dtkLogger::instance().attachFile(d->parser->value(logfileOption));
    // }

    if (d->parser->values(envOption).size() == 0)
        d->parser->showHelp(-1);

    if (d->parser->isSet(listOption)) {
        qInfo() << "List of all parameters:";
        qInfo() << "-----------------------";
    }

    if(d->parser->isSet(stereoOption)) {

        qDebug() << Q_FUNC_INFO << "Stereo:" << d->parser->value(stereoOption);

        if (d->parser->value(stereoOption).toLower() == "anaglyph")
            d->stereo = dtkVrApplication::Anaglyph;
        else if (d->parser->value(stereoOption).toLower() == "quad")
            d->stereo = dtkVrApplication::Quad;
    } else {
        d->stereo = dtkVrApplication::Mono;
    }

    foreach (QString env, d->parser->values(envOption)) {

        QFile file(env);

        if (!file.exists()) {
            qWarning() << "ERROR: " << env << "does not exists";
            d->parser->showHelp(-1);
        }

        if (d->parser->isSet(listOption)) {
            qDebug() << "Parsing: " << env  ;
        }

        QSettings settings(env, QSettings::IniFormat);

        foreach (QString key, settings.allKeys()) {
            QString newkey = key;
            newkey.replace(QString(":"),QString("/"));
            d->config.insert(newkey, settings.value(key));
        }
    }

    foreach (QString arg, d->parser->values(optOption)) {

        if (arg.contains("=")) {
            int index   = arg.indexOf("=");
            QString key = arg.left(index);
            index       = arg.size() - index - 1 ;
            QString val = arg.right(index);

            key.replace(QString(":"),QString("/"));

            if (val.contains(",")) {
                QStringList qs;
                qs = val.split(",");
                d->config.insert(key, qs);
            } else {
                d->config.insert(key, val);
            }
        } else {
            arg.replace(QString(":"),QString("/"));
            d->config.insert(arg, true);
        }
    }

    foreach (QString key, d->config.keys()) {

        QString keyword;
        int index;

        if (key.contains("/")) {
            index = key.lastIndexOf("/");
            index = key.size() - index - 1;
            keyword = key.right(index);
        } else {
            keyword = key;
        }

        if (d->syntax.contains(keyword)) {
            switch (d->syntax.value(keyword)) {
            case QMetaType::QVector3D:
                if (d->config.value(key).toList().count() == 3) {
                    d->config.insert(key, QVector3D (d->config.value(key).toList().at(0).toDouble(),
                                                     d->config.value(key).toList().at(1).toDouble(),
                                                     d->config.value(key).toList().at(2).toDouble()));
                } else {
                    qWarning() << "WARNING: invalid value for key [" << key << "] (using QVector3D() instead)";
                    d->config.insert(key, QVector3D());
                }
                break;
            case QMetaType::QRect :
                if (d->config.value(key).toList().count() == 4) {
                    d->config.insert(key, QRect (d->config.value(key).toList().at(0).toDouble(),
                                                 d->config.value(key).toList().at(1).toDouble(),
                                                 d->config.value(key).toList().at(2).toDouble(),
                                                 d->config.value(key).toList().at(3).toDouble()));
                } else {
                    qWarning() << "WARNING: invalid value for key [" << key << "] (using QRect() instead)";
                    d->config.insert(key, QRect());
                }
                break;
            case QMetaType::QQuaternion :
                if (d->config.value(key).toList().count() == 4 ) {
                    d->config.insert(key, QQuaternion (d->config.value(key).toList().at(0).toDouble(),
                                                       d->config.value(key).toList().at(1).toDouble(),
                                                       d->config.value(key).toList().at(2).toDouble(),
                                                       d->config.value(key).toList().at(3).toDouble()));
                } else {
                    qWarning() << "WARNING: invalid value for key [" << key << "] (using QQuaternion() instead)";
                    d->config.insert(key, QQuaternion());
                }
                break;
            case QMetaType::Double :
                d->config.insert(key, d->config.value(key).toDouble());
                break;
            case QMetaType::Bool :
                d->config.insert(key, d->config.value(key).toBool());
                break;
            case QMetaType::QString :
                d->config.insert(key, d->config.value(key).toString());
                break;
            default:
                qWarning() << Q_FUNC_INFO << "No syntax checker for type: " << d->syntax.value(keyword) ;
                break;
            }
        }
    }

    if (d->parser->isSet(listOption)) {
        qInfo() << "-----------------------" ;
        foreach (QString k, d->config.keys()) {
            qInfo() << k << " : " << d->config.value(k) ;
        }
        this->exit(0);
    }

    QMap<QString, bool> trackerName;

    foreach (QString key, d->config.keys()) {

        if(key.startsWith("tracker/"))  {

            QStringList split = key.split("/");

            if ((split.count() > 1) && (!trackerName.contains(split.at(1)))) {
                trackerName.insert(split.at(1), true);
            }
        }
    }

    if (trackerName.count() == 0) {
        qWarning() << Q_FUNC_INFO << "WARNING: no tracker found in .ini files";
    }

    foreach (QString name, trackerName.keys()) {

        QString   key;
        QString   value;

        key="tracker/"+name+"/input";

        qDebug() << "Configuring tracker:" << key;

        if (d->config.contains(key)) {

            value = d->config.value(key).toString();

            qDebug() << "-- found config infos for tracker:" << key << "(" << qPrintable(value) << ")";

            if (value == "none" || value == "")
                qDebug() << "-- tracker will not be used" ;

            dtkVrTracker *tracker = nullptr;
#if defined(DTKVR_HAS_VRPN)
            if (value.contains("Glasses@")) {
                tracker = new dtkVrTrackerVrpnVicon;
                tracker->setUrl(QString(value));
                qDebug() <<  "--" << QString(value) << "found" ;
            }
            if (value.contains("Flystick@")) {
                tracker = new dtkVrTrackerVrpnVicon;
                tracker->setUrl(QString(value));
            }
            if (value.contains("DTrackCW@")) {
                tracker = new dtkVrTrackerVrpnArt;
                tracker->setUrl(QString(value));
            }
            if (value.contains("DTrack@")) {
                tracker = new dtkVrTrackerVrpnArt;
                tracker->setUrl(QString(value));
            }
#endif
#if defined(DTKVR_HAS_OPENCV)
            if (value == "Laptop") {
                tracker = new dtkVrTrackerCamera;
            }
#endif
            if (tracker) {
                key = "tracker/" + name + "/cummulative";

                if (d->config.contains(key))
                    tracker->setCummulative(d->config.value(key).value<bool>());

                key = "tracker/" + name + "/sensitivity";

                if (d->config.contains(key))
                    tracker->setSensitivity(d->config.value(key).value<float>());
                else
                    qWarning() << "WARNING: key " << key << " does not exists or has uncorrect data (using default)";

                key = "tracker/" + name + "/behavior";

                if (d->config.contains(key)) {

                    value = d->config.value(key).toString();

                    qDebug() << Q_FUNC_INFO << key << value;

                    dtkVrBehaviors::instance()->associate(tracker, value);
                }

                tracker->initialize();

                d->trackers << tracker;

            } else {
                qWarning() << "WARNING:" << key << "is defined but the tracker is not implemented";
            }
        } else {
            qDebug() << "-- no infos tracker:" << key;
        }
    }
}

void dtkVrApplication::initializeGL(int argc, char **argv)
{
    QSurfaceFormat format;
// #if defined(Q_OS_MAC)
//     format.setMajorVersion(4);
//     format.setMinorVersion(5);
//     format.setProfile(QSurfaceFormat::CoreProfile);
// #endif
    format.setAlphaBufferSize(8);
    format.setRedBufferSize(8);
    format.setGreenBufferSize(8);
    format.setBlueBufferSize(8);
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setSamples(8);
// #if !defined(Q_OS_MAC)
//     for(int i = 1; i < argc; i++)
//         if(QString(argv[i]) == "quad" || QString(argv[i]) == "anaglyph")
//             format.setStereo(true);
// #endif
    format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
#if !defined(Q_OS_MAC)
    format.setOption(QSurfaceFormat::DebugContext);
#endif

    QSurfaceFormat::setDefaultFormat(format);
}

dtkVrApplication::StereoMode dtkVrApplication::stereo(void)
{
    return d->stereo;
}

QList<dtkVrTracker *> dtkVrApplication::trackers(void)
{
    return d->trackers;
}

dtkVrRenderer *dtkVrApplication::renderer(void)
{
    return d->renderer;
}

QVariant dtkVrApplication::config(QString key)
{
    if(this->isSet(key)) {

        return d->config.value(key);
    } else {
        qWarning() << "WARNING: key [" << key << "] has not been set";
        return QVariant();
    }
}

bool dtkVrApplication::isSet(QString key)
{
    return d->config.contains(key) ? true : false;
}

//
// dtkVrApplication.cpp ends here
