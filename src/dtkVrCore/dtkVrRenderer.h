// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtOpenGL>

class dtkVrTracker;

class dtkVrRenderer : public QObject, protected QOpenGLExtraFunctions
{
    Q_OBJECT

public:
             dtkVrRenderer(void);
    virtual ~dtkVrRenderer(void);

// /////////////////////////////////////////////////////////////////////////////
// NOTE: In process
// /////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Stereo mode definition

    double interocularDistance(void);

    QVector3D scenePosition(void);
    QQuaternion sceneOrientation(void);
    QVector3D sceneScale(void);

    QVector3D headPosition(void);
    QQuaternion headOrientation(void);

#pragma mark -
#pragma mark Setup

    void setHeadPosition(QVector3D position);
    void setHeadOrientation(QQuaternion orientation);
    void setInterocularDistance(double distance);
    void setScenePosition(QVector3D position);   // modify this to have a coherent API
    void setSceneOrientation(QQuaternion orientation);
    void setSceneScale(QVector3D scale);
    void setHeadTracker(dtkVrTracker *tracker);
    void setDevcTracker(dtkVrTracker *tracker);

    bool drawingLeftEye(void);
    bool drawingRightEye(void);

#pragma mark -
#pragma mark Scene initial attributes

    QVector3D   sceneOffsetPosition(void);
    QQuaternion sceneOffsetOrientation(void);
    QVector3D   sceneOffsetScale(void);

    void        setSceneOffsetPosition(QVector3D position);
    void        setSceneOffsetOrientation(QQuaternion orientation);
    void        setSceneOffsetScale(QVector3D scale);

// /////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Behavior

    virtual void   initializeGL(void);
    virtual void uninitializeGL(void);
    virtual void         drawGL(void) = 0;
    virtual void        paintGL(void);
    virtual void       resizeGL(int, int);

#pragma mark -
#pragma mark Events

    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent *);
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void keyPressEvent(QKeyEvent *);

// /////////////////////////////////////////////////////////////////////////////
// NOTE: In progress
// /////////////////////////////////////////////////////////////////////////////

protected:
    enum FrustumCoordinates {
        FRUSTUM_LEFT   = 0,
        FRUSTUM_RIGHT  = 1,
        FRUSTUM_BOTTOM = 2,
        FRUSTUM_TOP    = 3,
        FRUSTUM_NEAR   = 4,
        FRUSTUM_FAR    = 5
    };

protected:
    QVector3D screenUpperLeft(void);
    QVector3D screenLowerLeft(void);
    QVector3D screenLowerRight(void);

public:
    QMatrix4x4 model(void);
    QMatrix4x4 view(void);
    QMatrix4x4 projection(void);

// /////////////////////////////////////////////////////////////////////////////

private:
    class dtkVrRendererPrivate *d; friend class dtkVrRendererPrivate;
};

//
// dtkVrRenderer.h ends here
