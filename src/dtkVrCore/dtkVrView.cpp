// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrApplication.h"
#include "dtkVrApplication_p.h"
#include "dtkVrRenderer.h"
#include "dtkVrView.h"

#include <dtkFonts>

#include <dtkVr/dtkVrConfig.h>

#include <dtkVrCore/dtkVrApplication.h>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkVrViewPrivate
{
public:
    dtkVrView::instanciator instanciator;
    dtkVrRenderer *r = 0;

public:
    bool down = false;
    QPoint pos;

public:
    bool stats = true;

public:
    QString vendor;
    QString renderer;
    QString version;
    QString profile;
    QString options;

public:
    int frames = 0;
    QElapsedTimer time;

// /////////////////////////////////////////////////////////////////////////////
// NOTE: In process
// /////////////////////////////////////////////////////////////////////////////

public:
      QVector3D initial_scene_position;
    QQuaternion initial_scene_orientation;
      QVector3D initial_scene_scale;
      QVector3D initial_head_position;

public:
    void printIniFile(void);

// /////////////////////////////////////////////////////////////////////////////
};

void dtkVrViewPrivate::printIniFile(void)
{
    qInfo() << ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;";
    qInfo() << ";; \n;; ini file\n;;";
    qInfo() << ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;";
    qInfo() << "";
    qInfo() << "[scene]" ;
    qInfo() << "position        ="
            << r->scenePosition().x()
            << ","
            << r->scenePosition().y()
            << ","
            << r->scenePosition().z();
    qInfo() << "orientation     ="
            << r->sceneOrientation().scalar()
            << ","
            << r->sceneOrientation().x()
            << ","
            << r->sceneOrientation().y()
            << ","
            << r->sceneOrientation().z();
    qInfo() << "scale           ="
            << r->sceneScale().x()
            << ","
            << r->sceneScale().y()
            << ","
            << r->sceneScale().z();
    qInfo() << "";

    qInfo() << "[head]" ;
    qInfo() << "position        ="
            << r->headPosition().x()
            << ","
            << r->headPosition().y()
            << ","
            << r->headPosition().z();
    qInfo() << "orientation     ="
            << r->headOrientation().scalar()
            << ","
            << r->headOrientation().x()
            << ","
            << r->headOrientation().y()
            << ","
            << r->headOrientation().z();
    qInfo() << "";
    qInfo() << ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;";
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkVrView::dtkVrView(dtkVrView::instanciator instanciator, QWindow *parent) : QOpenGLWindow(QOpenGLWindow::NoPartialUpdate, parent)
{
    d = new dtkVrViewPrivate;
    d->instanciator = instanciator;

    dtkFontAwesome::instance()->initFontAwesome();

    QTimer *timer = new QTimer(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(update()));

    timer->start(1000/240);

    this->setTitle("dtkVR");
}

dtkVrView::~dtkVrView(void)
{
    delete d;
}

void dtkVrView::initializeGL(void)
{
    this->initializeOpenGLFunctions();

    d->r = d->instanciator();
    d->r->initializeGL();

    dtkVrApp->d->renderer = d->r;

    d->vendor = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    d->renderer = reinterpret_cast<const char*>(glGetString(GL_RENDERER));
    d->version = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    if(QSurfaceFormat::defaultFormat().profile() == QSurfaceFormat::CoreProfile)
         d->profile = "Core";
    if(QSurfaceFormat::defaultFormat().profile() == QSurfaceFormat::CompatibilityProfile)
         d->profile = "Compatibility";
    if(QSurfaceFormat::defaultFormat().options() & QSurfaceFormat::StereoBuffers)
         d->options = "Stereo";
    else
         d->options = "Mono";
    if(QSurfaceFormat::defaultFormat().options() & QSurfaceFormat::DebugContext)
         d->options += " | Debug";
    if(QSurfaceFormat::defaultFormat().options() & QSurfaceFormat::DeprecatedFunctions)
         d->options += " | Deprecated";

    qDebug() << Q_FUNC_INFO << "OpenGL         vendor:" << d->vendor;
    qDebug() << Q_FUNC_INFO << "OpenGL       renderer:" << d->renderer;
    qDebug() << Q_FUNC_INFO << "OpenGL implementation:" << d->version;
    qDebug() << Q_FUNC_INFO << "OpenGL        version:" << QSurfaceFormat::defaultFormat().version();
    qDebug() << Q_FUNC_INFO << "OpenGL        profile:" << QSurfaceFormat::defaultFormat().profile();
    qDebug() << Q_FUNC_INFO << "OpenGL        options:" << QSurfaceFormat::defaultFormat().options();

    // qDebug() << Q_FUNC_INFO << QSurfaceFormat::defaultFormat();

    if (dtkVrApp->isSet("scene/position")) {
        d->initial_scene_position = dtkVrApp->config("scene/position").value<QVector3D>();
    } else {
        qWarning() << Q_FUNC_INFO << "WARNING: scene/position not initialized (in .ini file or command line)";
        d->initial_scene_position = QVector3D();
    }

    if (dtkVrApp->isSet("scene/orientation")) {
        d->initial_scene_orientation = dtkVrApp->config("scene/orientation").value<QQuaternion>();
    } else {
        qWarning() << Q_FUNC_INFO << "WARNING: scene/orientation not initialized (in .ini file or command line)";
        d->initial_scene_orientation = QQuaternion();
    }

    d->initial_scene_orientation.normalize();

    if (dtkVrApp->isSet("scene/scale")) {
        d->initial_scene_scale = dtkVrApp->config("scene/scale").value<QVector3D>();
    } else {
        qWarning() << Q_FUNC_INFO << "WARNING: scene/scale not initialized (in .ini file or command line)";
        d->initial_scene_scale = QVector3D(1.0, 1.0, 1.0) ;
    }

    if (dtkVrApp->isSet("head/position")) {
        d->initial_head_position = dtkVrApp->config("head/position").value<QVector3D>();
    } else {
        qWarning() << Q_FUNC_INFO << "WARNING: head/position not initialized (in .ini file or command line)";
        d->initial_head_position = QVector3D();
    }

    d->r->setScenePosition(d->initial_scene_position);
    d->r->setSceneOrientation(d->initial_scene_orientation);
    d->r->setSceneScale(d->initial_scene_scale);
    d->r->setHeadPosition(d->initial_head_position);
}

void dtkVrView::paintGL(void)
{
// /////////////////////////////////////////////////////////////////////////////
// TODO: Com'on
// /////////////////////////////////////////////////////////////////////////////

    // QPainter painter;
    // painter.begin(this);
    // painter.beginNativePainting();

// /////////////////////////////////////////////////////////////////////////////

    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);

    d->r->paintGL();

    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LINE_SMOOTH);

// /////////////////////////////////////////////////////////////////////////////
// TODO: Com'on
// /////////////////////////////////////////////////////////////////////////////

//     painter.endNativePainting();

// // /////////////////////////////////////////////////////////////////////////////
// // Cursor overlay
// // /////////////////////////////////////////////////////////////////////////////

//     if (d->down) {
//          painter.setRenderHints(QPainter::Antialiasing, true);
//          painter.setPen(QPen(Qt::white, 3));
//          painter.setBrush(QColor(255, 0, 0, 128));
//          painter.drawEllipse(d->pos, 50, 50);
//     }

// // /////////////////////////////////////////////////////////////////////////////
// // Stats overlay
// // /////////////////////////////////////////////////////////////////////////////

//     if (d->stats) {

//         painter.setRenderHints(QPainter::Antialiasing, true);

//         QPixmap icon;

//                dtkFontAwesome::instance()->setDefaultOption("color", QColor(Qt::white));
//         icon = dtkFontAwesome::instance()->icon(fa::dashboard).pixmap(16, 16);

//         painter.setPen(Qt::white);

//         painter.drawPixmap(10, 5+2, icon);
//         painter.drawText(30, 20, "Vendor: " + d->vendor);

//         painter.drawPixmap(10, 25+2, icon);
//         painter.drawText(30, 40, "Driver: " + d->renderer);

//         painter.drawPixmap(10, 45+2, icon);
//         painter.drawText(30, 60, "Version: " + d->version);

//                dtkFontAwesome::instance()->setDefaultOption("color", QColor(128, 128, 255));
//         icon = dtkFontAwesome::instance()->icon(fa::dashboard).pixmap(16, 16);

//         painter.setPen(QColor(128, 128, 255));

//         painter.drawPixmap(10, 65+2, icon);
//         painter.drawText(30, 80, "Profile: " + d->profile);

//         painter.drawPixmap(10, 85+2, icon);
//         painter.drawText(30, 100, "Options: " + d->options);

//         // /////////////////////////////////////////////////////////////////////////////
//         // FPS
//         // /////////////////////////////////////////////////////////////////////////////

//         const int elapsed = d->time.elapsed();

//         QString fps;
//         fps.setNum(d->frames /(elapsed / 1000.0), 'f', 2);

//         dtkFontAwesome::instance()->setDefaultOption("color", QColor(128, 255, 128));

//         icon = dtkFontAwesome::instance()->icon(fa::dashboard).pixmap(16, 16);

//         painter.setPen(QColor(128, 255, 128));
//         painter.drawPixmap(10, 105+2, icon);
//         painter.drawText(30, 120, "FPS: " + fps);

//         dtkFontAwesome::instance()->setDefaultOption("color", QColor(255, 128, 128));

//         icon = dtkFontAwesome::instance()->icon(fa::gamepad).pixmap(16, 16);

//         painter.setPen(QColor(255, 128, 128));
//         painter.drawPixmap(10, 420, icon);
//         painter.drawText(30, 433, "No device found");

//         dtkFontAwesome::instance()->setDefaultOption("color", QColor(255, 255, 255));

//         QPixmap pixmap(QString(DTKVR_SOURCE_DIR + "/assets/inria.png"));

//         painter.setPen(QColor(255, 255, 255));
//         painter.drawPixmap(580, 365, pixmap.scaledToHeight(75));

//         // /////////////////////////////////////////////////////////////////////////////

//         if(!(d->frames % 100)) {
//             d->time.start();
//             d->frames = 0;
//         }

//         ++d->frames;
//     }

//     painter.end();
}

void dtkVrView::resizeGL(int width, int height)
{
    QOpenGLWindow::resizeGL(width * this->devicePixelRatio(),
                           height * this->devicePixelRatio());

    d->r->resizeGL(width * this->devicePixelRatio(), height * this->devicePixelRatio());
}

void dtkVrView::mouseMoveEvent(QMouseEvent *event)
{
    d->r->mouseMoveEvent(event);

    d->pos = event->pos();

    this->update();
}

void dtkVrView::mousePressEvent(QMouseEvent *event)
{
    d->r->mousePressEvent(event);

    d->down = true;
    d->pos = event->pos();

    this->update();
}

void dtkVrView::mouseReleaseEvent(QMouseEvent *event)
{
    d->r->mouseReleaseEvent(event);

    d->down = false;
    d->pos = event->pos();

    this->update();
}

void dtkVrView::keyPressEvent(QKeyEvent *event)
{
    d->r->keyPressEvent(event); // user defined keystrokes (by overloading dtkVrRenderer class)

    // standard keystrokes
    if ( event->modifiers() == Qt::NoModifier) {
        switch( event->key()) {
        case Qt::Key_Escape :       // quit the app
        case Qt::Key_Q :
            qApp->quit();
            break;
        case Qt::Key_S:             // toggle stats display
            d->stats = ! d->stats;
            break;
        case Qt::Key_P:             // context information
            d->printIniFile();
            break;

            // object translation
        case Qt::Key_I:
        case Qt::Key_Up:
            d->r->setScenePosition( d->r->scenePosition() + QVector3D( 0, 0.5, 0));
            break;
        case Qt::Key_K:
        case Qt::Key_Down:
            d->r->setScenePosition( d->r->scenePosition() - QVector3D( 0, 0.5, 0));
            break;
        case Qt::Key_J:
        case Qt::Key_Left:
            d->r->setScenePosition( d->r->scenePosition() - QVector3D( 0.5, 0, 0));
            break;
        case Qt::Key_L:
        case Qt::Key_Right:
            d->r->setScenePosition( d->r->scenePosition() + QVector3D( 0.5, 0, 0));
            break;
        case Qt::Key_U:
            d->r->setScenePosition( d->r->scenePosition() - QVector3D( 0, 0, 0.5));
            break;
        case Qt::Key_O:
            d->r->setScenePosition( d->r->scenePosition() + QVector3D( 0, 0, 0.5));
            break;

            // zoom
        case Qt::Key_BracketLeft:
            d->r->setSceneScale(d->r->sceneScale() / 2.0);
            break;
        case Qt::Key_BracketRight:
            d->r->setSceneScale(d->r->sceneScale() * 2.0);
            break;

        // reset
        case Qt::Key_R:
            d->r->setScenePosition(d->initial_scene_position);
            d->r->setSceneOrientation(d->initial_scene_orientation);
            d->r->setSceneScale(d->initial_scene_scale);
            d->r->setInterocularDistance(6.5);
            break;

            // help
        case Qt::Key_H:
            qInfo() << "";
            qInfo() << "Keystrokes:" ;
            qInfo() << "- move   scene   : arrows, J/L (x-axis), I/K (y-axis), U O (z-axis)" ;
            qInfo() << "- rotate scene   : CTRL/Command with arrows, J/L, I/K, U/O" ;
            qInfo() << "- scale  scene   : [ ]";
            //qInfo() << "- stereo mode    : S / M";
            //qInfo() << "- interocular dst: Y / CTRL-Y";
            qInfo() << "- reset  all     : R";
            qInfo() << "- print  infos   : P";
            qInfo() << "- quit app       : Q / ESC" ;
        }
    }

    if (event->modifiers() & Qt::ControlModifier) { // CTRL-key
        switch( event->key()) {
            // object rotation
        case Qt::Key_I:
        case Qt::Key_Up:
            d->r->setSceneOrientation(d->r->sceneOrientation() * QQuaternion::fromAxisAndAngle(1, 0, 0, -2.0));
            break;
        case Qt::Key_K:
        case Qt::Key_Down:
            d->r->setSceneOrientation(d->r->sceneOrientation() * QQuaternion::fromAxisAndAngle(1, 0, 0, 2.0));
            break;
        case Qt::Key_J:
        case Qt::Key_Left:
            d->r->setSceneOrientation(d->r->sceneOrientation() * QQuaternion::fromAxisAndAngle(0, 1, 0, -2.0));
            break;
        case Qt::Key_L:
        case Qt::Key_Right:
            d->r->setSceneOrientation(d->r->sceneOrientation() * QQuaternion::fromAxisAndAngle(0, 1, 0, 2.0));
            break;
        case Qt::Key_U:
            d->r->setSceneOrientation(d->r->sceneOrientation() * QQuaternion::fromAxisAndAngle(0, 0, 1, 2.0));
            break;
        case Qt::Key_O:
            d->r->setSceneOrientation(d->r->sceneOrientation() * QQuaternion::fromAxisAndAngle(0, 0, 1, -2.0));
            break;

        }
    }
    this->update();
}

//
// dtkVrView.cpp ends here
