// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVrApplication.h"
#include "dtkVrBehavior.h"
#include "dtkVrRenderer.h"
#include "dtkVrTracker.h"

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkVrBehavior::dtkVrBehavior(QString name, dtkVrBehavior::behaviorHandler behavior)
{
    dtkVrBehaviors::instance()->record(name, behavior);
}

dtkVrBehavior::~dtkVrBehavior(void)
{

}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkVrBehaviorsPrivate
{
public:
    QHash<QString, dtkVrBehavior::behaviorHandler> dictionnary;

public:
    QHash<dtkVrTracker *, QString> association;
};

dtkVrBehaviors *dtkVrBehaviors::s_instance = NULL;

dtkVrBehaviors *dtkVrBehaviors::instance(void)
{
    if(!s_instance)
        s_instance = new dtkVrBehaviors;

    return s_instance;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void moveHead(dtkVrTracker *, dtkVrRenderer *);
void moveScene(dtkVrTracker *, dtkVrRenderer *);
void debugTracker(dtkVrTracker *, dtkVrRenderer *);
void debugOrientation(dtkVrTracker *, dtkVrRenderer *);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkVrBehaviors::dtkVrBehaviors(void)
{
    d = new dtkVrBehaviorsPrivate ;

    this->record("moveHead", moveHead);
    this->record("moveScene", moveScene);
    this->record("debugTracker", debugTracker);
    this->record("debugOrientation", debugOrientation);
}

dtkVrBehaviors::~dtkVrBehaviors(void)
{
    delete d ;
}

bool dtkVrBehaviors::invoque(dtkVrTracker *tracker, dtkVrRenderer *renderer, bool silent)
{
    if (!d->association.keys().contains(tracker)) {

        if (!silent)
            qWarning() << Q_FUNC_INFO << "WARNING: no behavior associated to this tracker";

        return false;
    }

    if (!d->dictionnary.keys().contains(d->association.value(tracker))) {

        if (!silent)
            qWarning() << Q_FUNC_INFO << "WARNING: the behavior (" << d->association.value(tracker)<< ") is not properly defined";
    }

    dtkVrBehavior::behaviorHandler funcName = d->dictionnary.value(d->association.value(tracker));

    funcName(tracker, renderer);

    return true;
}

bool dtkVrBehaviors::associate(dtkVrTracker *tracker, QString behavior)
{
    if (!tracker) {
        qWarning() << Q_FUNC_INFO << "ERROR: please provide a valid tracker instead of NULL";
        return false;
    }

    if (!d->dictionnary.keys().contains(behavior)) {
        qWarning() << Q_FUNC_INFO << "WARNING: the behavior (" << behavior << ") has not yet been defined.";
    }

    if (d->association.keys().contains(tracker)) {
        qWarning() << Q_FUNC_INFO << "INFO: this tracker was already associated with " << behavior;
    }

    d->association[tracker] = behavior;

    qDebug() << Q_FUNC_INFO << "association:" << d->association;
    qDebug() << Q_FUNC_INFO << "dictionnary:" << d->dictionnary;

    return true;
}

bool dtkVrBehaviors::record(QString name, dtkVrBehavior::behaviorHandler func)
{
    if (name.isEmpty()) {
        qWarning() << Q_FUNC_INFO << "ERROR: please provide a valid not null key";
        return false;
    }

    if (d->dictionnary.keys().contains(name)) {
        qWarning() << Q_FUNC_INFO
                   << "WARNING: behavior ("
                   << name << ") already defined. Ignoring the new one.";

        return false;
    }

    qDebug() << "INFO: behavior " << name << "added.";

    d->dictionnary[name] = func ;

    return true;
}

// /////////////////////////////////////////////////////////////////////////////
// Handlers implementation
// /////////////////////////////////////////////////////////////////////////////

void moveHead(dtkVrTracker *tracker, dtkVrRenderer *renderer)
{
    QVector3D   position;
    QQuaternion orientation;
    bool        cummulative;
    float       sensitivity;

    if (tracker) {
        position    = QVector3D(tracker->position());
        orientation = QQuaternion(tracker->orientation());
        cummulative = tracker->cummulative();
        sensitivity = tracker->sensitivity();
    } else {
        qDebug() << "No tracker";
    }

    if(!renderer)
        return;

    if (cummulative) {
        orientation.setScalar(0);
        renderer->setHeadPosition(renderer->headPosition() + position * sensitivity);
        renderer->setHeadOrientation(renderer->headOrientation() + orientation);
    } else {
        renderer->setHeadPosition(position);
        renderer->setHeadOrientation(orientation);
    }
}

void debugOrientation(dtkVrTracker *tracker, dtkVrRenderer *renderer)
{
    QQuaternion orientation;
    bool        cummulative;
    float       sensitivity;

    if (tracker) {
        orientation = QQuaternion(tracker->orientation());
        cummulative = tracker->cummulative();
        sensitivity = tracker->sensitivity();
    } else {
        qDebug() << "No tracker";
    }

    if(!renderer)
        return;

    if (cummulative) {
        orientation.setScalar(0);
        renderer->setSceneOrientation(renderer->sceneOrientation() + orientation);
    } else {
        renderer->setSceneOrientation(orientation);
        qDebug() << "BEHAVIOR" << orientation;
    }
}

void moveScene(dtkVrTracker *tracker, dtkVrRenderer *renderer)
{
    QVector3D   position;
    QQuaternion orientation;
    bool        cummulative;
    float       sensitivity;

    if (tracker) {
        position    = QVector3D(tracker->position());
        orientation = QQuaternion(tracker->orientation());
        cummulative = tracker->cummulative();
        sensitivity = tracker->sensitivity();
    }

    if(!renderer)
        return;

    if (cummulative) {
        renderer->setScenePosition(renderer->scenePosition()  + position*sensitivity);
        orientation.setScalar(0);
        renderer->setSceneOrientation(renderer->sceneOrientation()  + orientation);
    } else {
        renderer->setScenePosition(position);
        renderer->setSceneOrientation(orientation);
    }
}

void debugTracker(dtkVrTracker *tracker, dtkVrRenderer *renderer)
{
    QVector3D   position;
    QQuaternion orientation;
    bool        cummulative;
    float       sensitivity;

    if (tracker) {
        position    = QVector3D(tracker->position());
        orientation = QQuaternion(tracker->orientation());
        cummulative = tracker->cummulative();
        sensitivity = tracker->sensitivity();
        qDebug() << "=== Tracker:" << tracker->url();
        qDebug() << "Position   :" << position;
        qDebug() << "Orientation:" << orientation;
    } else {
      qDebug() << "No tracker";
    }
}

//
// dtkVrBehavior.cpp ends here
