// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtOpenGL>

#include "dtkVrRenderer.h"

class dtkVrView : public QOpenGLWindow, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    typedef dtkVrRenderer *(*instanciator)(void);

public:
      dtkVrView(instanciator, QWindow * = nullptr);
     ~dtkVrView(void);

protected:
    void initializeGL(void)     override;
    void      paintGL(void)     override;
    void     resizeGL(int, int) override;

protected:
    void mouseMoveEvent(QMouseEvent *)    override;
    void mousePressEvent(QMouseEvent *)   override;
    void mouseReleaseEvent(QMouseEvent *) override;

protected:
    void keyPressEvent(QKeyEvent *)   override;

private:
    class dtkVrViewPrivate *d;
};

template <typename T> dtkVrRenderer *instanciator(void)
{
    return new T;
};

//
// dtkVrView.h ends here
